@echo off

call environment.bat

cd /d "%~dp0"
git pull 2>NUL
if %ERRORLEVEL% == 0 goto :done

git reset --hard
git pull

:done
pause